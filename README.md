# DayZ - Fandom Wiki Foundry VTT Module

This module contains content curated from the [DayZ Fandom Wiki](https://dayz.fandom.com/wiki/DayZ_Wiki) with community content provided here by the [DayZ Fandom Wiki](https://dayz.fandom.com/wiki/DayZ_Wiki)'s [CC BY-NC-SA](https://www.fandom.com/licensing) license, for use with the Foundry VTT TTRPG system.

The author(s) of this module make no claims of ownership to or responsibility for any creative content provided through this module as published by the wiki.

## Artwork

This module contains artwork uploaded to and provided by the [DayZ Fandom Wiki](https://dayz.fandom.com/wiki/DayZ_Wiki).

Artwork not expressly copyrighted is assumed to have been made available to the Public Domain or at most through the [CC BY-NC-SA](https://creativecommons.org/licenses/by-sa/3.0/legalcode) license.

We attribute artists, where such information exists, and uploaders of such artwork to the Wiki. Thank you, [Lasyen](https://dayz.fandom.com/wiki/UserProfile:Lasyen), and [Swatsy07](https://dayz.fandom.com/wiki/User:Swatsy07) for your service!

### The DayZ Logo
As stated in the wiki, the DayZ logo was taken from the [Steam Store page](http://store.steampowered.com/app/221100) for DayZ. The DayZ logo is property of [Bohemia Interactive a.s.](https://www.bistudio.com/), which this module is not affiliated with in any way.

#### Licensing
The DayZ logotype image comes from the video game this wiki is concerned with or from websites created and owned by its publisher, the copyright of which is held by the publisher or developer of the game. All trademarks and registered trademarks present in the image are proprietary to the publisher of the game. For more, see the copyright notice.
The use of images to illustrate articles concerning the subject of the images in question is believed to qualify as fair use under United States copyright law, as such display does not significantly impede the right of the copyright holder to sell the copyrighted material.

## Technical Considerations

For the content to be appealing for use with Foundry VTT, the author(s) of this module have taken the following liberties.

### Artwork Resampling

To minimize artwork file sizes and for compatibility, all artwork has been converted to WebP format and canvases resized to 1000:1618 aspect ratio with 5px margin. Long items depicted horisontally in artwork have been rotated -27 degrees within the canvas for visibility and appeal. Renaming of image files has been done to lowercase for categorization and ease of identification.
