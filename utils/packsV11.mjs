import { Level } from "level";
import crypto from "crypto";
import fs from "fs";
import gulp from "gulp";
import logger from "fancy-log";
import mergeStream from "merge-stream";
import path from "path";
import through2 from "through2";
import yargs from "yargs";

/**
 * Parsed arguments passed in through the command line.
 * @type {object}
 */
const parsedArgs = yargs(process.argv).argv;

/**
 * Folder where the compiled compendium packs should be located relative to the
 * base 5e system folder.
 * @type {string}
 */
const PACK_DEST = "packs";

/**
 * Folder where source JSON files should be located relative to the 5e system folder.
 * @type {string}
 */
const PACK_SRC = "packs/src";

const PACK_TYPES = {
  "Actor": "actors",
  "Item": "items",
  "Scene": "scenes"
};

/**
 * Cache of DBs so they aren't loaded repeatedly when determining IDs.
 * @type {Object<string,Datastore>}
 */
const DB_CACHE = {};


/* ----------------------------------------- */
/*  Clean Packs
/* ----------------------------------------- */

/**
 * Removes unwanted flags, permissions, and other data from entries before extracting or compiling.
 * @param {object} data  Data for a single entry to clean.
 * @param {object} [options]
 * @param {boolean} [options.clearSourceId]  Should the core sourceId flag be deleted.
 */
function cleanPackEntry(data, { clearSourceId=true }={}) {
}

/**
 * Return a random alphanumerical string of length len
 * There is a very small probability (less than 1/1,000,000) for the length to be less than len
 * (il the base64 conversion yields too many pluses and slashes) but
 * that's not an issue here
 * The probability of a collision is extremely small (need 3*10^12 documents to have one chance in a million of a collision)
 * See http://en.wikipedia.org/wiki/Birthday_problem
 */
function uid (len) {
  return crypto.randomBytes(Math.ceil(Math.max(8, len * 2)))
    .toString('base64')
    .replace(/[+\/]/g, '')
    .slice(0, len);
}

/**
 * Attempts to find an existing matching ID for an item of this name, otherwise generates a new unique ID.
 * @param {object} data        Data for the entry that needs an ID.
 * @param {string} pack        Name of the pack to which this item belongs.
 * @returns {Promise<string>}  Resolves once the ID is determined.
 */
function determineId (data, pack) {
  const db_path = path.join(PACK_DEST, `${pack}.db`);
  if ( !DB_CACHE[db_path] ) {
    DB_CACHE[db_path] = new Datastore({ filename: db_path, autoload: true });
    DB_CACHE[db_path].loadDatabase();
  }
  const db = DB_CACHE[db_path];

  return new Promise((resolve, reject) => {
    db.findOne({ name: data.name }, (err, entry) => {
      if ( entry ) {
        resolve(entry._id);
      } else {
        resolve(db.createNewId());
      }
    });
  });
}

/**
 * Removes invisible whitespace characters and normalises single- and double-quotes.
 * @param {string} str  The string to be cleaned.
 * @returns {string}    The cleaned string.
 */
function cleanString (str) {
  return str.replace(/\u2060/gu, "").replace(/[‘’]/gu, "'").replace(/[“”]/gu, '"');
}

/**
 * Cleans and formats source JSON files, removing unnecessary permissions and flags
 * and adding the proper spacing.
 *
 * - `gulp cleansePacks` - Clean all source JSON files.
 * - `gulp cleansePacks --pack classes` - Only clean the source files for the specified compendium.
 * - `gulp cleansePacks --pack classes --name Barbarian` - Only clean a single item from the specified compendium.
 */
function cleansePacks () {
  const packName = parsedArgs.pack;
  const entryName = parsedArgs.name?.toLowerCase();
  const folders = fs.readdirSync(PACK_SRC, { withFileTypes: true }).filter(file =>
    file.isDirectory() && ( !packName || (packName === file.name) )
  );

  const packs = folders.map(folder => {
    logger.info(`Cleaning pack ${folder.name}`);
    gulp.src(path.join(PACK_SRC, folder.name, "/*.folder"))
      .pipe(through2.obj(async (file, enc, callback) => {
        const file_str = file.contents.toString()
        logger.info(file_str);
        const json = JSON.parse(file_str);
        const name = json.name.toLowerCase();
        if ( entryName && (entryName !== name) ) return callback(null, file);
        cleanPackEntry(json);
        if ( !json._id ) json._id = await determineId(json, folder.name);
        fs.rmSync(file.path, { force: true });
        fs.writeFileSync(file.path, `${JSON.stringify(json, null, 2)}\n`, { mode: 0o664 });
        callback(null, file);
      }));
    return gulp.src(path.join(PACK_SRC, folder.name, "/**/*.json"))
      .pipe(through2.obj(async (file, enc, callback) => {
        const json = JSON.parse(file.contents.toString());
        const name = json.name.toLowerCase();
        if ( entryName && (entryName !== name) ) return callback(null, file);
        cleanPackEntry(json);
        if ( !json._id ) json._id = await determineId(json, folder.name);
        fs.rmSync(file.path, { force: true });
        fs.writeFileSync(file.path, `${JSON.stringify(json, null, 2)}\n`, { mode: 0o664 });
        callback(null, file);
      }));
  });

  return mergeStream(packs);
}
export const cleanse = cleansePacks;

/* ----------------------------------------- */
/*  Compile Packs
/* ----------------------------------------- */

function getPackTypes () {
  const data = fs.readFileSync('module.json', 'utf8');
  const json = JSON.parse(data);
  const packs = json['packs'].map(pack => [
    pack.path.match(/^.*\/([A-Za-z0-9_-]+)(?:\..*)?$/)[1],
    PACK_TYPES[pack.type]
  ]);
  return Object.fromEntries(packs);
}

/**
 * Compile the source JSON files into compendium packs.
 *
 * - `gulp compilePacksV11` - Compile all JSON files into their NEDB files.
 * - `gulp compilePacksV11 --pack classes` - Only compile the specified pack.
 */
function compilePacksV11() {
  const packName = parsedArgs.pack;
  const pack_type = getPackTypes();
  // Determine which source folders to process
  const folders = fs.readdirSync(PACK_SRC, { withFileTypes: true }).filter(file =>
    file.isDirectory() && ( !packName || (packName === file.name) )
  );
  const packs = folders.map(folder => {
    // logger.info(folder.name, pack_type, pack_type[folder.name]);
    const filePath = path.join(PACK_DEST, folder.name);
    // fs.rmSync(filePath, { force: true });
    const db = new Level(filePath, { valueEncoding: 'json' });
    db.clear();
    const folder_data = [];
    logger.info(`Compiling pack ${folder.name}`);
    gulp.src(path.join(PACK_SRC, folder.name, "/**/*.folder"))
      .pipe(through2.obj((file, enc, callback) => {
        const json = JSON.parse(file.contents.toString());
        cleanPackEntry(json);
        folder_data.push(json);
        callback(null, file);
      }, callback => {
        folder_data.sort((lhs, rhs) => lhs.name > rhs.name ? 1 : -1);
        folder_data.forEach(entry => db.put(`!folders!${entry._id}`, entry));
        callback();
      }));
    const entry_data = [];
    return gulp.src(path.join(PACK_SRC, folder.name, "/**/*.json"))
      .pipe(through2.obj((file, enc, callback) => {
        const json = JSON.parse(file.contents.toString());
        cleanPackEntry(json);
        entry_data.push(json);
        callback(null, file);
      }, callback => {
        entry_data.sort((lhs, rhs) => lhs.name > rhs.name ? 1 : -1);
        entry_data.forEach(entry => db.put(`!${pack_type[folder.name]}!${entry._id}`, entry));
        db.close();
        callback();
      }));
  });
  return mergeStream(packs);
}
export const compile = compilePacksV11;


/* ----------------------------------------- */
/*  Extract Packs
/* ----------------------------------------- */

/**
 * Extract the contents of compendium packs to JSON files.
 *
 * - `gulp extractPacks` - Extract all compendium NEDB files into JSON files.
 * - `gulp extractPacks --pack classes` - Only extract the contents of the specified compendium.
 * - `gulp extractPacks --pack classes --name Barbarian` - Only extract a single item from the specified compendium.
 */
function extractPacks() {
}
export const extract = extractPacks;
