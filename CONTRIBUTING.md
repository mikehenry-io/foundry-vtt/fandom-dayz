# Contributing

Contributers who want to provide content, updates, and corrections from the DayZ Fandom Wiki are welcome to submit merge-requests.

This module contains content packs for the GDW House Rules system. We welcome merge-requests for packs also supporting other game systems.
